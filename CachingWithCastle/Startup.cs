using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CachingWithCastle.QCaching;
using Autofac;
using System.Reflection;
using Autofac.Extensions.DependencyInjection;
using Autofac.Extras.DynamicProxy;
using CachingWithCastle.Services.CachingWithCastle.Services;

namespace CachingWithCastle
{
    public class Startup
    {
        #region 启用拦截
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            //完成我们的注入和启用拦截操作
            services.AddScoped<ICachingProvider, MemoryCachingProvider>();

            return this.GetAutofacServiceProvider(services);
        }
        /// <summary>
        /// AutoFac 注册
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private IServiceProvider GetAutofacServiceProvider(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.Populate(services);
            var assembly = this.GetType().GetTypeInfo().Assembly;
            builder.RegisterType<QCachingInterceptor>();
            //scenario 1
            builder.RegisterAssemblyTypes(assembly)
                         .Where(type => typeof(IDateTimeService).IsAssignableFrom(type) && !type.GetTypeInfo().IsAbstract)
                         .AsImplementedInterfaces()
                         .InstancePerLifetimeScope()
                         .EnableInterfaceInterceptors()
                         .InterceptedBy(typeof(QCachingInterceptor));

            return new AutofacServiceProvider(builder.Build());
        }
        #endregion
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        //// This method gets called by the runtime. Use this method to add services to the container.
        //public void ConfigureServices(IServiceCollection services)
        //{
        //    services.AddMvc();
        //}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            //注册路由
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
