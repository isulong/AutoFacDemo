﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CacheWebForm.Services
{
 
        public interface IDateTimeService
        {
            string GetCurrentUtcTime();
        }

    public class DateTimeService : IDateTimeService
    {
        [QCache.QCaching(AbsoluteExpiration = 10)]
        public string GetCurrentUtcTime()
        {
            return System.DateTime.UtcNow.ToString();
        }
    }
}