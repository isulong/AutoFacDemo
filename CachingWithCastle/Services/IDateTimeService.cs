﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CachingWithCastle.Services
{
    namespace CachingWithCastle.Services
    {
        public interface IDateTimeService
        {
            string GetCurrentUtcTime();
        }

        public class DateTimeService : IDateTimeService  
        {
            [QCaching.QCaching(AbsoluteExpiration = 10)]
            public string GetCurrentUtcTime()
            {
                return System.DateTime.UtcNow.ToString();
            }
        }
    }
}
