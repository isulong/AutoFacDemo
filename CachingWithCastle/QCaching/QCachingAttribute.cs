﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CachingWithCastle.QCaching
{
    /// <summary>
    /// 定义一个特性，添加缓存并设置属性，如过期时间
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class QCachingAttribute : Attribute
    {
        public int AbsoluteExpiration { get; set; } = 30;

        //add other settings ...
    }
}
