﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CachingWithCastle.Services.CachingWithCastle.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CachingWithCastle.Controllers
{
    public class HomeController : Controller
    {
        private IDateTimeService _dateTimeService;
        public HomeController(IDateTimeService dateTimeService)
        {
            _dateTimeService = dateTimeService;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return Content(_dateTimeService.GetCurrentUtcTime());
        }
    }
}
