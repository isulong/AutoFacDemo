﻿ 

namespace CacheWebForm.QCache
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Caching;

    /// <summary>
    /// 实现缓存接口的MemoryCache版本
    /// </summary>
    public class MemoryCachingProvider : ICachingProvider
    {
        
        public MemoryCachingProvider()
        {
        }

        public object Get(string cacheKey)
        {
            return CacheHelper.GetCache(cacheKey);
        }

        public async Task<object> GetAsync(string cacheKey)
        {
            return await Task.FromResult<object>(CacheHelper.GetCache(cacheKey));
        }

        public void Set(string cacheKey, object cacheValue, TimeSpan absoluteExpirationRelativeToNow)
        {

            CacheHelper.SetCache(cacheKey, cacheValue, absoluteExpirationRelativeToNow);
        }

        public async Task SetAsync(string cacheKey, object cacheValue, TimeSpan absoluteExpirationRelativeToNow)
        {
            await Task.Run(() =>
            {
                CacheHelper.SetCache(cacheKey, cacheValue, absoluteExpirationRelativeToNow);
            });
        }
    }
}