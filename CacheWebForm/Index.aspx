﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="CacheWebForm.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Literal ID ="showdata" runat ="server">

            </asp:Literal>
            <asp:Button ID="GetData" Text="获取数据" OnClick="GetData_Click" runat="server"/>
        </div>
    </form>
</body>
</html>
