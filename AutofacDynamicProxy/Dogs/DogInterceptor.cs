﻿/************************************************************************************
 * Copyright (c) 2018 China All Rights Reserved.
 * CLR版本： 4.0.30319.42000
 * 机器名称：HZWNB147-PC
 * 唯一标识：2411a59a-7ee0-4437-a226-f5608dfc31bb
 * 创建人：  HZWNB147
 * 创建时间：2018/3/27 9:07:13
 * 描述：
 * 
 * 
 ************************************************************************************/
using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutofacDynamicProxy.Dogs
{
    //AutoFac的动态代理
    public class DogInterceptor : IInterceptor
    {
        #region 代理类添加IDog的实现
        private readonly IDog _dog;

        /// <summary>
        /// 通过依赖注入 注入ICat的具体实现
        /// </summary>
        /// <param name="cat"></param>
        public DogInterceptor(IDog dog)
        {
            _dog = dog;
        } 
        #endregion

        public void Intercept(IInvocation invocation)
        {
            //Console.WriteLine("狗吃东西之前");
            //invocation.Proceed();
            //Console.WriteLine("狗吃东西之后");
            Console.WriteLine("喂狗吃东西");
            invocation.Method.Invoke(_dog, invocation.Arguments);//调用Dog的指定方法
        }
    }
}
