﻿namespace CachingWithAspectCore
{
    using AspectCore.Configuration;
    using AspectCore.Extensions.DependencyInjection;
    using AspectCore.Injector;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Linq;
    using System.Reflection;
    using CachingWithAspectCore.QCaching;
    using CachingWithAspectCore.Services;
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddScoped<ICachingProvider, MemoryCachingProvider>();
            services.AddScoped<IDateTimeService, DateTimeService>();
            //services.AddSingleton(factory =>
            //{
            //    Func<string, IDateTimeService> accesor = key =>
            //    {
            //        if (key.Equals("DateTimeService"))
            //        {
            //            return factory.GetService<DateTimeService>();
            //        }
            //        else if (key.Equals("DateTimeService2"))
            //        {
            //            return factory.GetService<DateTimeService2>();
            //        }
            //        else
            //        {
            //            throw new ArgumentException($"Not Support key : {key}");
            //        }
            //    };
            //    return accesor;
            //});
            //handle BLL class
            var assembly = this.GetType().GetTypeInfo().Assembly;
            this.AddBLLClassToServices(assembly, services);

            var container = services.ToServiceContainer();
            container.AddType<QCachingInterceptor>();
            container.Configure(config =>
            {
                config.Interceptors.AddTyped<QCachingInterceptor>(method => typeof(IQCaching).IsAssignableFrom(method.DeclaringType));
            });

            return container.Build();
        }
        public void AddBLLClassToServices(Assembly assembly, IServiceCollection services)
        {
            var types = assembly.GetTypes().ToList();

            foreach (var item in types.Where(x => x.Name.EndsWith("BLL", StringComparison.OrdinalIgnoreCase) && x.IsClass))
            {
                services.AddSingleton(item);
            }
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
