﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CachingWithAspectCore.BLL
{
    public class DateTimeBLL : QCaching.IQCaching
    {
        [QCaching.QCaching(AbsoluteExpiration = 10)]
        public virtual string GetCurrentUtcTime()
        {
            return System.DateTime.UtcNow.ToString();
        }
    }
}
