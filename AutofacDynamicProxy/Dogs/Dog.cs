﻿/************************************************************************************
 * Copyright (c) 2018 China All Rights Reserved.
 * CLR版本： 4.0.30319.42000
 * 机器名称：HZWNB147-PC
 * 唯一标识：83edd847-8081-4bce-a158-9abb7c6fecf5
 * 创建人：  HZWNB147
 * 创建时间：2018/3/27 9:05:27
 * 描述：
 * 
 * 
 ************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace AutofacDynamicProxy.Dogs
{
    public class Dog:IDog
    {
        public void Eat()
        {
            Console.WriteLine("狗在吃东西");
        }
    }
}
