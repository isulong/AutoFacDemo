﻿/************************************************************************************
 * Copyright (c) 2018 China All Rights Reserved.
 * CLR版本： 4.0.30319.42000
 * 机器名称：HZWNB147-PC
 * 唯一标识：0353c696-5ab9-4219-a86e-ca2e8c293f11
 * 创建人：  HZWNB147
 * 创建时间：2018/3/27 8:56:01
 * 描述：
 * 
 * 
 ************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace AutofacDynamicProxy.Cats
{
    /// <summary>
    /// 静态代理
    /// </summary>
    public class CatProxy : ICat
    {
        private readonly ICat _cat;
        public CatProxy(ICat cat)
        {
            _cat = cat;
        }
        public void Eat()
        {
            Console.WriteLine("猫吃东西之前");
            _cat.Eat();
            Console.WriteLine("猫吃东西之后");
        }
    }
}
