﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutofacDynamicProxy.Dogs
{
    public interface IDog
    {
        void Eat();
    }
}
