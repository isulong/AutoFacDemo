﻿/************************************************************************************
 * Copyright (c) 2018 China All Rights Reserved.
 * CLR版本： 4.0.30319.42000
 * 机器名称：HZWNB147-PC
 * 唯一标识：1365c0aa-7b7b-449d-b8c3-1515f371e322
 * 创建人：  HZWNB147
 * 创建时间：2018/3/27 8:55:40
 * 描述：
 * 
 * 
 ************************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace AutofacDynamicProxy.Cats
{
    public class Cat : ICat
    {
        public void Eat()
        {
            Console.WriteLine("猫在吃东西");
        }
    }
}
