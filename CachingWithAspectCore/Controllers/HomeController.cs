﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CachingWithAspectCore.Controllers
{
    using AspectCore.Injector;
    using CachingWithAspectCore.BLL;
    using CachingWithAspectCore.Services;
    using Microsoft.AspNetCore.Mvc;

    public class HomeController : Controller
    {
        private IDateTimeService _dateTimeService;
        //private IDateTimeService _dateTimeService2;
        //private readonly Func<string, IDateTimeService> _serviceAccessor;
        public HomeController(IDateTimeService dateTimeService)
        {

            _dateTimeService = dateTimeService;
        }
        //public HomeController(Func<string, IDateTimeService> serviceAccessor)
        //{
        //    this._serviceAccessor = serviceAccessor;

        //    _dateTimeService = _serviceAccessor("DateTimeService");
        //    _dateTimeService2 = _serviceAccessor("DateTimeService2");
        //}

        public IActionResult Index()
        {
            //var ddd = _dateTimeService.GetCurrentUtcTime();
            //var ddwwd = _dateTimeService2.GetCurrentUtcTime();
            return Content(_dateTimeService.GetCurrentUtcTime());
        }
    }
        public class BllController : Controller
    {
        private IServiceResolver _scope;
        private DateTimeBLL _dateTimeBLL;

        public BllController(IServiceResolver scope)
        {
            this._scope = scope;
            _dateTimeBLL = _scope.Resolve<DateTimeBLL>();
        }

        public IActionResult Index()
        {
            return Content(_dateTimeBLL.GetCurrentUtcTime());
        }
    }
}