﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//参考站点：http://autofac.readthedocs.io/en/latest/getting-started/index.html
namespace AutoFacConApp
{
    /*
     * 定义特征：描述工具的特征，即用接口定义方法 
     * 定义工具：完成工具的定义，即实现接口
     * 容器注册：容器相当于一个工具箱，里面可以放一些扳手，钳子等工具。【容器的注册】
     * 依赖注入：完成一件事需要从容器中取工具，取工具需要告诉容器工具的特征（接口描述），即依赖注入
     * 获得工具：将规则告诉容器，容器返回一个满足规则的实例【容器的解析对象】
     */
    class Program
    {
        //控制器翻转IOC，接口相当于定义一套规则，将规则告诉容器，容器返回实现规则的实例
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ConsoleOutput>().As<IOutput>();
            builder.RegisterType<TodayWriter>().As<IDateWriter>();

            Container = builder.Build();

            // The WriteDate method is where we'll make use
            // of our dependency injection. We'll define that
            // in a bit.
            WriteDate();
            Console.ReadKey();
        }
        public static void WriteDate()
        {
            
            // Create the scope, resolve your IDateWriter,
            // use it, then dispose of the scope.
            using (var scope = Container.BeginLifetimeScope())
            {
                var writer = scope.Resolve<IDateWriter>();
                writer.WriteDate();
            }
        }
    }
}
