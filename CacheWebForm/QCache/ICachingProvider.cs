﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CacheWebForm.QCache
{
    public interface ICachingProvider
    {
        object Get(string cacheKey);
        void Set(string cacheKey, object cacheValue, TimeSpan absoluteExpirationRelativeToNow);
    }
}