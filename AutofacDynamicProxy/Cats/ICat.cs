﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutofacDynamicProxy.Cats
{
    public interface ICat
    {
        void Eat();
    }
}
