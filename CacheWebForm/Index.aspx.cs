﻿using Autofac;
using CacheWebForm.QCache;
using CacheWebForm.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CacheWebForm
{
    public partial class Index : Page
    {
        public IDateTimeService dateTimeService { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GetData_Click(object sender, EventArgs e)
        {
            GetData.Text = dateTimeService.GetCurrentUtcTime();
        }
    }
}